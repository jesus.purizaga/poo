public class Docente {
    private String nombre;
    private Integer edad;
    public static Integer cantidad;

    public static String imprimir(){
        return "de acuerdo";
    }

    public Docente(String nombre, Integer edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public Docente() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }
}
