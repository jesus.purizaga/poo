package pe.edu.uni.fiis.polimorfismo;

public class Hp extends Impresora
        implements Eficiente{
    public String imprimir(){
        return "HP";
    }

    public Integer potenciar() {
        return 80;
    }
}
