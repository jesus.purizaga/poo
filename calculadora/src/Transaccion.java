import org.w3c.dom.ls.LSOutput;

public class Transaccion {
    public static void main(String[] args) {
        //Producto p = new Producto();
        Producto producto=new Producto(
                10.50f, 15, "Pilot",
                "Board Master", "verde") ;
        /*
        producto.setColor("verde");
        producto.setMarca("Pilot");
        producto.setModelo("Board Master");
        producto.setPrecio(10.50f);
        producto.setStock(15);*/
        for (int i = 0; i < 10 && producto.getStock()>0; i++) {
            int cantidad = 1+(int)(Math.random()*10);
            vender(producto,cantidad);
        }
    }

    Maestro maestro = new Maestro();
    /*maestro.setNombre("Chavarri");

    Docente.cantidad =20;
    Maestro.cantidad =45;
    System.out.println(Docente.imprimir());
    Docente docente = new Docente();
    docente.setEdad(12);*/

    public static void vender(Producto producto,int cantidad){
        if(producto.getStock()>= cantidad) {
            Venta venta = new Venta();
            venta.setCantidad(cantidad);
            venta.setProducto(producto);
            venta.setIgv(0.18f
                    * venta.getProducto().getPrecio()
                    * venta.getCantidad()
            );
            venta.setMontoTotal(
                    venta.getIgv()
                            + venta.getProducto().getPrecio()
                            * venta.getCantidad()
            );
            venta.imprimir();
            producto.setStock(producto.getStock() - venta.getCantidad());
        }
    }
}
